import React from 'react'

const NameList = () => {
    const numbers = [
        {
          "id": 1,
          "name": "John Doe",
          "age": 30,
          "skill": "Web Development"
        },
        {
          "id": 2,
          "name": "Jane Smith",
          "age": 25,
          "skill": "Graphic Design"
        },
        {
          "id": 3,
          "name": "Bob Johnson",
          "age": 35,
          "skill": "Data Analysis"
        },
        {
          "id": 4,
          "name": "Sarah Wilson",
          "age": 28,
          "skill": "Mobile App Development"
        },
        {
          "id": 5,
          "name": "Michael Brown",
          "age": 32,
          "skill": "Machine Learning"
        },
        {
          "id": 6,
          "name": "Emily Davis",
          "age": 27,
          "skill": "Content Writing"
        },
        {
          "id": 7,
          "name": "David Lee",
          "age": 29,
          "skill": "Digital Marketing"
        },
        {
          "id": 8,
          "name": "Olivia Clark",
          "age": 31,
          "skill": "UI/UX Design"
        },
        {
          "id": 9,
          "name": "William White",
          "age": 26,
          "skill": "Database Administration"
        },
        {
          "id": 10,
          "name": "Ava Harris",
          "age": 33,
          "skill": "Project Management"
        }
      ]
      
  return (
    <div>
        {
            numbers.map((number , inx )=> 
                    <p key={inx}>{number.age}</p> 
                    )
        }
    </div>
  ) 
}



export default NameList