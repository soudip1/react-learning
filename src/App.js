// import logo from './logo.svg';
import './App.css';
// import Todo from './components/Todo';
// import Greet from './components/Greet';
// import Message from './message';
// import Welcome from './components/Welcome';
// import Hello from './components/Hello';
// import Counter from './components/Counter';
// import Colorpicker from './components/Colorpicker';
// import Colourpicker from './components/Colourpicker';
// import Parentcomponent from './Parentcomponent';
// import UserGreeting from './UserGreeting';
// import NameList from './NameList';
// import Stylesheet from './components/Stylesheet';
// import Form from './components/Form';
// import User from './components/User';
// import HoookCounterThree from './components/HoookCounterThree';
// import EffectHookOne from './components/EffectHookOne';
// import HookMOuse from './components/HookMOuse';
// import Mousecontaier from './components/Mousecontaier';
// import Intervalhookcount from './components/Intervalhookcount';
// import Youtubeform from './components/Youtubeform';
// import HookForm from './components/HookForm';

import { Routes, Route } from "react-router-dom";
import Home from "./components/Home";
// import About from "./components/About";

import Navbar from "./components/Navbar";
import Ordersummary from './components/Ordersummary';
import Nomatch from './components/Nomatch';
import Product from './components/Product';
import Featuredproducts from './components/Featuredproducts';
import Newproducts from './components/Newproducts';
import Users from './components/Users';
import Userdetail from './components/Userdetail';
import Admin from './components/Admin';
import React from 'react';
import Profile from './components/Profile';
const Lazyabout = React.lazy(() => import("./components/About"))
function App() {
  return (
    // <div className="App">
    //   {/* <HookForm /> */}
    //   {/* <Youtubeform />  */}
    //   {/* <Intervalhookcount /> */}
    //   {/* <Mousecontaier /> */}
    //   {/* <HookMOuse /> */}
    //   {/* <EffectHookOne /> */}
    //   {/* <HoookCounterThree /> */}
    //   {/* <User render={(isLogIn) => isLogIn ?  'saha' : 'Tufan'} /> */}
    //   {/* <Form /> */}
    //   {/* <Stylesheet primary = {true} /> */}
    //   {/* {<NameList />} */}
    //   {/* <UserGreeting /> */}
    //   {/* <Parentcomponent /> */}
    //   {/* <Greet name="Tanu" surname="saha">
    //     <h2>
    //       +917477597751</h2>
    //   </Greet>
    //   <Greet name="Soudip" surname="saha">
    //     <button>Test</button>
    //   </Greet> */}
    //   {/* <Message /> */}
    //   {/* <Welcome name="Soudip" surname="saha"/>
    //   <Welcome name="Tanu" surname="saha" />
    //   <Welcome name="Tufan" surname="saha"/> */}
    //   {/* <Hello /> */}
    //   {/* <Counter addvalue={2} subvalue={1}/> */}
    //   {/* <Todo /> */}
    //   {/* <Colorpicker />
    //   <Colourpicker /> */}
    // </div>
    <>
      <Navbar />
      <Routes>
        <Route path='/' element={<Home />}></Route>
        <Route path='/profile' element={<Profile />}></Route>
        {/* <Route path='/about' element={<About />}></Route> */}
        {/* LAzy loading */}
        <Route path='/about' element={
                                        <React.Suspense fallback='loading....'>
                                          <Lazyabout />
                                        </React.Suspense>
                                      }></Route>

        <Route path='order-summary' element={<Ordersummary />}></Route>
        <Route path='*' element={<Nomatch />}></Route>
        <Route path='products' element={<Product />}>
          <Route index element={<Featuredproducts />}></Route>
          <Route path='featured' element={<Featuredproducts />}></Route>
          <Route path='new' element={<Newproducts />}></Route>
        </Route>
        <Route path='user' element={<Users />}>
          <Route path=':userId' element={<Userdetail />}></Route>
          <Route path='admin' element={<Admin />}></Route>
        </Route>

      </Routes>
    </>


  );
}

export default App;
