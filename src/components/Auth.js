import React, { useState , createContext , useContext } from 'react';
export const AuthProvider = ({children}) => {
    const Authcontext = createContext(null)
    const [user, setUser] = useState()
    const login = (user) => {
        setUser(user)
    }
    const logout = (user) =>{
        setUser(null)
    }
    return (
        <Authcontext.Provider value={{user ,login , logout }}>
            {children}
        </Authcontext.Provider>
    );
};

export const useAuth = () =>{
    return useContext(Authcontext)
}