import React, { useState } from "react";
import Table from "../Table";
import "./style.css";

const initialitems = {
  index: -1,
  firstname: "",
  lastname: "",
  gender: "",
  dob: "",
  subject: "",
  summary: "",
};
function Todo(props) {
  const [items, setItems] = useState(initialitems);
  const changeHandler = (e) => {
    const { name, value } = e.target;
    setItems({
      ...items,
      [name]: value,
    });
  };
  const [savevalue, setSavevalue] = useState([]);

  const editHandeler = (index) => {
    var edittedObject = [...savevalue][index];
    const editedFields = {
      index: index,
      firstname: edittedObject.firstname,
      lastname: edittedObject.lastname,
      gender: edittedObject.gender,
      dob: edittedObject.dob,
      subject: edittedObject.subject,
      summary: edittedObject.summary,
    };
    setItems(editedFields);
  };

  const handeldelete = (index) => {
    const updatedValues = [...savevalue];
    updatedValues.splice(index, 1);
    setSavevalue(updatedValues);
    if(items.index === index){
      setItems(initialitems);
    }
  };

  const onSubmit = (e) => {
    e.preventDefault();
    if (items.index === -1) {
      setSavevalue([...savevalue, items]);
    } else {
      let updateValue = savevalue;
      updateValue[items.index] = items;
      setSavevalue(updateValue);
    }
    setItems(initialitems);
  };

  return (
    <div className="main-div">
      <div className="to-do-container">
        <h1>To-do list</h1>
        <form className="to-do-foerm" onSubmit={onSubmit}>
          <div className="row">
            <div className="span6">
              <label>
                First Name
                <input
                  id="first-name"
                  name="firstname"
                  value={items.firstname}
                  onChange={changeHandler}
                  type="text"
                />
              </label>
            </div>
            <div className="span6">
              <label>
                Last Name
                <input
                  id="first-name"
                  name="lastname"
                  value={items.lastname}
                  onChange={changeHandler}
                  type="text"
                />
              </label>
            </div>
          </div>

          <div className="row">
            <div className="span3">
              <label>Gender</label>
              <div className="radio" >
                <div className="radio">
                  <input
                    type="radio"
                    name="gender"
                    value="male"
                    checked={items.gender === "male"}
                    onChange={changeHandler}
                  />
                  <div className="radio-label">
                  Male
                  </div>
                </div>
                <div  className="radio">
                  <input
                    type="radio"
                    name="gender"
                    value="Female"
                    checked={items.gender === "Female"}
                    onChange={changeHandler}
                  />
                  <div className="radio-label">
                  Female
                  </div>
                </div>
              </div>
            </div>
            <div className="span3">
              <label>
                Date of Birth
                <input
                  type="date"
                  id="dob"
                  name="dob"
                  value={items.dob}
                  onChange={changeHandler}
                />
              </label>
            </div>
            <div className="span3">
              <label>
                To-Do Subject
                <select
                  id="subject"
                  name="subject"
                  value={items.subject}
                  onChange={changeHandler}
                >
                  <option value="">Select Subject</option>
                  <option value="Important">Important</option>
                  <option value="Less Important">Less Important</option>
                  <option value="Most Important">Most Important</option>
                </select>
              </label>
            </div>
          </div>

          <div className="span12">
            <label>
              Summary
              <textarea
                id="summary"
                name="summary"
                value={items.summary}
                onChange={changeHandler}
                rows={3}
                cols={30}
              />
            </label>
          </div>

          <button className="btn-default" type="submit">
            Submit
          </button>
        </form>
      </div>
      {savevalue.length >= 1 ? (
        <Table
          allValue={savevalue}
          editHandeler={editHandeler}
          handeldelete={handeldelete}
        />
      ) : (
        <div style={{ textAlign: "center"}}>
          <div class='image'>
          <img
            src="../no-data.png"
            alt="Description of the Image"
            style={{width: '100%'}}
          />
          </div>
          <p className="red">No Data Found!</p>
        </div>
      )}
    </div>
  );
}

export default Todo;
