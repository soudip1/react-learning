import React from 'react'
import './style.css'

const Table = (props) => {
    let allValue = props.allValue
    const editHandeler = props.editHandeler
    const handeldelete = props.handeldelete

  return (
    <div className='to-do-table'>
    <table>
       <tr>
         <th>Full Name</th>
         <th>Gender</th>
         <th>DOB</th>
         <th>Subject</th>
         <th>Summary</th>
         <th>Actions</th>
       </tr>
        {allValue.map((data,index) =>
        <tr>
            <td>{data.firstname + ' ' +data.lastname }</td>
            <td>{data.gender}</td>
            <td>{data.dob}</td>
            <td>{data.subject}</td>
            <td>{data.summary}</td>
            <td>
            <button className='btn-primary' onClick={()=>editHandeler(index)}>Edit</button>
            <button className='btn-danger' onClick={()=>handeldelete(index)}>Delete</button>
            </td>
        </tr>
        )}
    </table>
  </div>
  )
}

export default Table