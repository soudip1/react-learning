import React from 'react'
import { DevTool } from '@hookform/devtools';
import { useForm,useFieldArray } from 'react-hook-form'

let renderCount = 0
const Youtubeform = () => {
    const form = useForm({
        // defaultValues: {
        //     username: '',
        //     email: '',
        //     channel: '',
        //     social:{
        //         twitter:'',
        //         linkedin:''
        //     }
        // }

    }) //returns object
    const { register, control, handleSubmit, formState , watch , reset , trigger} = form  
    const { errors , isDirty , isValid} = formState

    const onSubmit = (data) => {
        console.log('Form submitted', data)
    }
    renderCount++
    const watcUsername = watch("username")

    return (
        <div className='container'>
            <h2 style={{ color: '#8f6cfb' }}>Youtube Form ({renderCount / 2})</h2>
            <h2 style={{ color: '#8f6cfb' }}>Watched value:{watcUsername}</h2>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className='form-group'>
                    <label htmlFor="username">Username:</label>
                    <input type="text" id="username" {...register("username", {
                        required: {
                            value: true,
                            message: 'Username is required'
                        },
                        validate: {
                            notAdmin: (fieldValue) => {
                                return fieldValue === 'soudip' ? 'Enter a different name' : true;
                            },
                            notBlackListed: (fieldValue) => {
                                return !fieldValue.endsWith('saha') ? true : 'Enter a different surname';
                            },
                        },

                    })} />
                    <p className="error">{errors.username?.message}</p>
                </div>

                <div className='form-group'>
                    <label htmlFor="email">E-Mail:</label>
                    <input type="email" id="email" {...register("email", {
                        required: 'Need Email',
                        pattern: {
                            value: /[A-Za-z0-9._%+-]+@[A-Za-z0-9.-]+\.[A-Z|a-z]{2,7}/,
                            message: 'Need Proper Email'
                        }
                    })} />
                    <p className="error">{errors.email?.message}</p>

                </div>

                <div className='form-group'>
                    <label htmlFor="channel">Channel:</label>
                    <input type="text" id="channel" {...register("channel", {
                        required: 'Need Channel Name'
                    })} />
                    <p className="error">{errors.channel?.message}</p>

                </div>

                <div className='form-group'>
                    <label htmlFor="age">Age:</label>
                    <input type="number" id="age" {...register("age", {
                        valueAsNumber: true,
                        required: 'Need age Name'
                    })} />
                    <p className="error">{errors.age?.message}</p>

                </div>

                <div className='form-group'>
                    <label htmlFor="dob">Date Of Birth:</label>
                    <input type="date" id="dob" {...register("dob", {
                        valueAsDate: true,
                        required: 'Need dob'
                    })} />
                    <p className="error">{errors.dob?.message}</p>

                </div>

                <div className='form-group'>
                    <label htmlFor="twitter">Twitter:</label>
                    <input type="text" id="twitter" {...register("social.twitter", {
                        required: 'Need Your Twitter Account'
                    })} />
                    <p className="error">{errors.social?.twitter?.message}</p>
                </div>

                <button type='button' onClick={() => reset()}>Reset</button>
                <button type='submit' >Submit</button>
                {/* disabled={!isDirty || !isValid} */}
            </form>
            <DevTool control={control} />

        </div>
    );
};

export default Youtubeform;