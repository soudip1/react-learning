import React, { Fragment } from 'react';
import { useForm, useFieldArray } from 'react-hook-form';

const UserInformation = ( {register , control} ) => {
    const { fields , append , remove} =  useFieldArray({
        control,
        name:"users"
    })
    return(
        <div className='inner-container'>
            <h2>User Informations</h2>
            {
                fields.map((item , index)=>{
                    return (
                            <div className="form-row form-group" key={item.id}>
                                <h4> {index + 1} item</h4>
                                <div className="col">
                                    <input type="text" className="form-control" placeholder="Enter Your First name" {...register(`users.${index}.fname`)} />
                                </div>
                                <div className="col">
                                    <input type="text" className="form-control" placeholder="Enter Your Last name" {...register(`users.${index}.lname`)} />
                                </div>
                                <div className="col">
                                    <input type="text" className="form-control" placeholder="Enter Your E-mail Address" {...register(`users.${index}.email`)} />
                                </div>
                                <div className="col">
                                    <select className="form-control" id="state" {...register(`users.${index}.state`)}>
                                    <option value="">Select Your State</option>
                                    <option value="Jharkhand">Jharkhand</option>
                                    <option value="Bihar">Bihar</option>
                                    <option value="Assam">Assam</option>
                                    <option value="Goa">Goa</option>
                                    <option value="Manipur">Manipur</option>
                                    </select>
                                </div>

                                <div className="col">
                                    <button type="button" className='btn-danger' onClick={() => remove(index)}>Remove</button>
                                </div>
                            </div>
                        )
                })
            }
                <div className='form-group'>
                    <button type='button' onClick={()=> append({users:""}) }>Add Data</button>
                </div>
        
        </div>
    )
}
const HookForm = () => {
    const { register, handleSubmit, formState ,control } = useForm()
    const { errors } = formState;

    const onSubmit = (data) => {
        console.log('Form data:', data);
    };

    return (
        <div className='container'>
            <form onSubmit={handleSubmit(onSubmit)}>
                <div className="form-group">
                    <label htmlFor="fullname">Full Name</label>
                    <input type="text" className="form-control" id="fullname" {...register("fullname",{
                        required:'Please give us your firstname'
                    })} />
                    <p className="error"> {errors.fullname?.message} </p>
                </div>
                <div className="form-group">
                    <label htmlFor="email">Email address</label>
                    <input type="email" className="form-control" id="email" {...register("email")} />
                </div>
                <div className="form-group">
                    <label htmlFor="phone">Phone Number</label>
                    <input type="text" className="for           m-control" id="phone" {...register("phone")} />
                </div>
                <div className="form-group">
                    <label htmlFor="password">Password</label>
                    <input type="password" className="form-control" id="password" {...register("password")} />
                </div>
                <UserInformation register={register} control={control}/>
               
                <button type="submit">Submit Data</button>
            </form>
        </div>
    );
};

export default HookForm;
