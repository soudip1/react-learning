import React from "react";
import { BlockPicker } from "react-color";
import { useState } from "react";

function Colourpicker() {
  //creating state to store our color and also set color using onChange event for block picker
  const [blockPickerColor, setBlockPickerColor] = useState("#faced1");
  const [color, setColor] = useState("#ffffff");

  const changeHandler = (e) => {
    setColor(e.target.value);
  };

  return (
    <div>
      <div className="blockpicker">
        <h2>Color Picker With blockpicker</h2>
        <div className="inner-div"
          style={{ display: "flex", flexDirection:"column" , alignItems: "center" }}
        >
          <div
            style={{
              backgroundColor: `${blockPickerColor}`,
              width: 100,
              height: 60,
              border: "2px solid white",
            }}
          ></div>
          <BlockPicker
            color={blockPickerColor}
            onChange={(color) => {
              setBlockPickerColor(color.hex);
            }}
          />
        </div>
      </div>
      {/* <div>
        <input
          id="color"
          name="color"
          value={color}
          onChange={changeHandler}
          type="color"
        />
        <div
          className="color-changing"
          style={{
            width: "100px",
            height: "100px",
            backgroundColor: color,
            marginTop: "20px",
          }}
        ></div>
        <p>Selected Color: {color}</p>
      </div> */}
    </div>
  );
}

export default Colourpicker;
