import React, { useState } from "react";
function Todo(props) {
  const [items, setItems] = useState([]);
  const [index, setindex] = useState(-1);
  const [firstname, setFirstname] = useState("");
  const [lastname, setLastname] = useState("");
  const [gender, setGender] = useState("");
  const [dob, setDob] = useState("");
  const [subject, setSubject] = useState("");
  const [summary, setSummary] = useState("");

  const firstnameChangeHandler = (e) => {
    setFirstname(e.target.value);
  };
  const lastnameChangeHandler = (e) => {
    setLastname(e.target.value);
  };
  const handleGenderChange = (e) => {
    setGender(e.target.value);
  };
  const dobChangeHandler = (e) => {
    setDob(e.target.value);
  };
  const subjectGenderChange = (e) => {
    setSubject(e.target.value);
  };
  const summaryChangeHandler = (e) => {
    setSummary(e.target.value);
  };
 
  const editHandeler = (itemId) => {
    const editedItems = [...items][itemId]
    var splittedName = editedItems.name.split(" ")
    setFirstname(splittedName[0]);
    setLastname(splittedName[1]);
    setGender(editedItems.gender);
    setDob(editedItems.dob);
    setSubject(editedItems.subject);
    setSummary(editedItems.summary);
    setindex(itemId)
  };

  const handeldelete = (itemId) => {
    const updatedItems = [...items]
    // const updatedItems = items.filter((item,index) => index !== itemId);
    updatedItems.splice(itemId, 1)
    setItems(updatedItems);
  };

  const onSubmit = (e) => {
    e.preventDefault()
    const newItem = {
      name: firstname + " " + lastname,
      gender: gender,
      dob: dob,
      subject: subject,
      summary: summary,
    };
    if(index === -1){
      setItems([...items, newItem]);
    }else{
      let updateitems = items
      updateitems[index]=newItem
      setItems(updateitems);
      setindex(-1)
    }
    
    setFirstname("");
    setLastname("");
    setGender("");
    setDob("");
    setSubject("");
    setSummary("");
  };

  // console.log(items);

  return (
    <div>
      <div>
        <h1>To-do list</h1>
        <form onSubmit={onSubmit}>
          <label>
            First Name:
            <input
              id="first-name"
              value={firstname}
              onChange={firstnameChangeHandler}
              type="text"
            />
          </label>
          <br />
          <br />

          <label>
            Last Name:
            <input
              id="first-name"
              value={lastname}
              onChange={lastnameChangeHandler}
              type="text"
            />
          </label>
          <br />
          <br />
          <label>
            Gender:
            <label>
              <input
                type="radio"
                value="male"
                checked={gender === "male"}
                onChange={handleGenderChange}
              />
              Male
            </label>
            <label>
              <input
                type="radio"
                value="Female"
                checked={gender === "Female"}
                onChange={handleGenderChange}
              />
              Female
            </label>
          </label>
          <br />
          <br />

          <label>
            Date of Birth:
            <input
              type="date"
              id="dob"
              value={dob}
              onChange={dobChangeHandler}
            />
          </label>
          <br />
          <br />

          <label>
            To-Do Subject:
            <select value={subject} onChange={subjectGenderChange}>
              <option value="">Select Subject</option>
              <option value="Important">Important</option>
              <option value="Less Important">Less Important</option>
              <option value="Most Important">Most Important</option>
            </select>
          </label>
          <br />
          <br />

          <label>
            Summary:
            <textarea
              value={summary}
              onChange={summaryChangeHandler}
              rows={4}
              cols={40}
            />
          </label>
          <br />
          <br />

          <button type="submit">Submit</button>
        </form>
      </div>

      {items.length >= 1 ? (
        <div style={{ textAlign: "center" }}>
          <table>
            <tr>
              <th>Full Name</th>
              <th>Gender</th>
              <th>DOB</th>
              <th>Subject</th>
              <th>Summary</th>
              <th>Actions</th>
            </tr>
          {items.map((data,index) =>
            <tr>
              <td>{data.name}</td>
              <td>{data.gender}</td>
              <td>{data.dob}</td>
              <td>{data.subject}</td>
              <td>{data.summary}</td>
              <td>
                <button onClick={()=>editHandeler(index)}>Edit</button>
                <button onClick={()=>handeldelete(index)}>Delete</button>
              </td>
            </tr>
            )}
          </table>
        </div>
      ) : (
        <div style={{ textAlign: "center" }}>
          <p>Please add data. </p>
        </div>
      )}
    </div>
  );
}

export default Todo;
