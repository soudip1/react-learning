import React from 'react'
import './myStyle.css'

const Stylesheet = ({primary}) => {
  let className = primary ? 'primary' : ''
  const heading = {
    fontSize: '50px',
    color: '#125896',
  }
  return (
    <>
    <div className={`${className} font-xl`}>Stylesheet</div>
    <h2 style={heading} >Soudip</h2>
    <p></p>
    </>
  )
}

export default Stylesheet