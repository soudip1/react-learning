import React from 'react';
import { useNavigate } from 'react-router-dom';

const Ordersummary = () => {
    const navigate = useNavigate()
    return (
        <div>
            Order Confirmed
            <button type='button' onClick={() => navigate(-1 ,  {replace:true} )  }>Go Back </button>
        </div>
    );
};



export default Ordersummary;