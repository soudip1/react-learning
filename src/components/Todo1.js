import React, { useState } from "react";
import Table from "./Table";
const initialitems = {
  index: -1,
  firstname: '',
  lastname:'',
  gender: '',
  dob: '',
  subject: '',
  summary: '',
}
const  Todo =(props)=>  {
  const [items, setItems] = useState(initialitems)
    const changeHandler = (e) => {
      const {name , value} = e.target
      setItems({
        ...items,
        [name]:value,
      })
    }
  const [savevalue, setSavevalue] = useState([])
  
  const editHandeler = (index) => {
    var edittedObject = [...savevalue][index]
    const editedFields = {
      index: index,
      firstname: edittedObject.firstname,
      lastname:edittedObject.lastname,
      gender: edittedObject.gender,
      dob: edittedObject.dob,
      subject: edittedObject.subject,
      summary: edittedObject.summary,
    }
    setItems(editedFields)
  }

  const handeldelete = (index) => {
    const updatedValues = [...savevalue]
    updatedValues.splice(index, 1)
    setSavevalue(updatedValues)
  }

  const onSubmit = (e) => {
    e.preventDefault()
    if(items.index === -1){
      setSavevalue([...savevalue , items])
      setItems(initialitems)
    }else{
      let updateValue = savevalue
      updateValue[items.index]=items
      setSavevalue(updateValue)
      setItems(initialitems)

    }
    
  }
  return (
    <div>
      <div>
        <h1>To-do list</h1>
        <form onSubmit={onSubmit}>
          <label>
            First Name:
            <input
              id="first-name"
              name="firstname"
              value={items.firstname}
              onChange={changeHandler}
              type="text"
            />
          </label>
          <br />
          <br />

          <label>
            Last Name:
            <input
              id="first-name"
              name="lastname"
              value={items.lastname}
              onChange={changeHandler}
              type="text"
            />
          </label>
          <br />
          <br />
          <label>
            Gender:
            <label>
              <input
                type="radio"
                name="gender"
                value="male"
                checked={items.gender === "male"}
                onChange={changeHandler}
              />
              Male
            </label>
            <label>
              <input
                type="radio"
                name="gender"
                value="Female"
                checked={items.gender === "Female"}
                onChange={changeHandler}
              />
              Female
            </label>
          </label>
          <br />
          <br />

          <label>
            Date of Birth:
            <input
              type="date"
              id="dob"
              name="dob"
              value={items.dob}
              onChange={changeHandler}
            />
          </label>
          <br />
          <br />

          <label>
            To-Do Subject:
            <select id="subject" name="subject" value={items.subject} onChange={changeHandler}>
              <option value="">Select Subject</option>
              <option value="Important">Important</option>
              <option value="Less Important">Less Important</option>
              <option value="Most Important">Most Important</option>
            </select>
          </label>
          <br />
          <br />

          <label>
            Summary:
            <textarea
              id="summary"
              name="summary"
              value={items.summary}
              onChange={changeHandler}
              rows={4}
              cols={40}
            />
          </label>
          <br />
          <br />

          <button type="submit">Submit</button>
        </form>
      </div>
      {savevalue.length >= 1 ? (
        <Table allValue = {savevalue} editHandeler = {editHandeler} handeldelete = {handeldelete}  />
      ) : (
        <div style={{ textAlign: "center" }}>
          <p>Please add data. </p>
        </div>
      )}
    </div>
  )
}

export default Todo;
