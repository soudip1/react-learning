import React , {useState} from 'react'

const HoookCounterThree = () => {
    // const [name , setName] = useState({fname:'' , lname:''})
    const [items, setItems] = useState([])
    const addItem = () => {
        setItems(
            [...items,
                {
                    id:items.length,
                    value: Math.floor(Math.random() * 10) + 1

                }
            ]
        )
    }
  return (
  <>
    <div>Set Value</div>
    <button onClick={addItem}>Add a number</button>
        {/* <form>
            <input type='text' value={name.fname} onChange={(e) => setName({...name , fname: e.target.value})} />
            <input type='text' value={name.lname} onChange={(e) => setName({...name ,lname: e.target.value})} />
        </form>

        <h2>{name.fname}</h2>
        <h2>{name.lname}</h2> */}
        <ul>
            {items.map( (item , inx) =>(
                <li key={item.id}>{item.value}</li>
            ))}
        </ul>
        
  </> 
  )
}

export default HoookCounterThree