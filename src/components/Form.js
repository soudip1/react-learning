import { useState } from "react";


const Form = () => {
    const [name, setName] = useState('')
    const [topic, setTopic] = useState('')
    const handelNameChange=(e) =>{
        setName(e.target.value)
    }
    const handelTopic=(e)=>{
        setTopic(e.target.value)
    }
    const handeSubmt = (e) =>{
        e.preventDefault()
    }
    

    return (
        <>
            <h1>Form</h1>
            <form onSubmit={handeSubmt}>
                <label>Name</label>
                <div>
                    <input style={{width:'50%' , display:'block' , margin:'auto'}} 
                                placeholder = 'Enter Your name' 
                                type='text' 
                                value={name}
                                onChange={handelNameChange}   />
                </div>
                <label>Topic</label>
                <div>
                    <select style={{width:'50%' , display:'block' , margin:'auto'}} value={topic} onChange={handelTopic}>
                        <option value='React'>React</option>
                        <option value='Angular'>Angular</option>
                        <option value='Node'>Node</option>
                        <option value='Express'>Express</option>
                    </select>
                </div>
                <button type='button' onClick={()=> console.log(name,'.............',topic)
}>Submit</button>
            </form>
        </>
    )

}
export default Form