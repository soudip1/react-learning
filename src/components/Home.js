import React from 'react';
import { useNavigate } from 'react-router-dom';
const Home = () => {
    const usenavigaate = useNavigate()
    return (
        <div>
            Home Page
            <button type='button' onClick={() => usenavigaate('order-summary' ,)}>Order Placed</button>
        </div>
    );
};

export default Home;