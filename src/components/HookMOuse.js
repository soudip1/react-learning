import React  , {useState , useEffect} from 'react';

const HookMOuse = () => {
    const [x, setX] = useState(0)
    const [y, setY] = useState(0)
    const catchmousemovement = (e) =>{
        console.log('mouse move catch')
        setX(e.clientX)
        setY(e.clientY)
    }
    useEffect(() => {
        console.log('call use effect')
        window.addEventListener('mousemove' , catchmousemovement)
    
    })
    
    return (
        <div>
            Hooks X- {x}
            Hokks Y - {y}
        </div>
    );
};

export default HookMOuse;