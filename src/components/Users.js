import React from 'react';
import { Link , Outlet ,useSearchParams } from 'react-router-dom';
const Users = () => {
const [searchParams, setSearchParams] = useSearchParams()
const showactiveusers =  searchParams.get('filter') === 'active'

    return (
        <div>
            <nav>
                {/* <Link to='soudip'>Soudip</Link>
                <Link to='ufan'>Tufan</Link>
                <Link to='kaka'>Kaka</Link>
                <Link to='admin'>admin</Link> */}
                <button onClick={() => setSearchParams({filter: 'active'}) }>Active User</button>
                <button onClick={() => setSearchParams() }>Reset</button>
            </nav>
            <div>
                {/* <Outlet /> */}
                {
                    showactiveusers ? (
                        <h2>Showing active users</h2>
                    ):(
                        <h2>Showing all users</h2>
                    )
                }
            </div>
        </div>
    );
};

export default Users;