import React from 'react';
import { NavLink } from "react-router-dom"

const Navbar = () => {
    return (
        <div>
            <nav className='primary-nav'>
                <h1>Test Project</h1>
                <NavLink to='/'>Home</NavLink>
                <NavLink to='/about'>About</NavLink>
                <NavLink to='/products'>Products</NavLink>
                <NavLink to='/profile'>Profile</NavLink>
            </nav>
        </div>
    );
};

export default Navbar;