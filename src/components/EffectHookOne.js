import React , {useState , useEffect} from 'react';

const EffectHookOne = () => {
    const [count, setCount] = useState(0)
    const [name , setName] = useState('')
    useEffect(() => {
        console.log('useeffect start');
        document.title = `you clicked ${count} times`
    } , [count] )
    return (
        <div>
            <input typr='text' value={name} onChange={(e) => (setName(e.target.value))}/>
            <button onClick = {() =>setCount(count + 1) }> Click {count} {(count < 2) ? 'time' : 'times'}</button>
            
        </div>
    );
};

export default EffectHookOne;