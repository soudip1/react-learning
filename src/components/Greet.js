import React from "react";
// const Greet = props =>{
//     console.log(props);
// }
function Greet(props) {
    // console.log(props);
    return <div>
            <h1>{props.name} {props.surname}</h1>
            <p>{props.children}</p>
            </div> 
}

export default Greet