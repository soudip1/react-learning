import React, { useState } from 'react'

// class Counter extends Component {
// constructor(props) {
//   super(props)

//   this.state = {
//      count:0
//   }
// }
// increment(){
//     // this.setState({
//     //     count: this.state.count + 1
//     // } , () =>{
//     //          console.log('Callback value' , this.state.count)
//     //         }
//     // )
    

//     this.setState(prevstate =>({
//         count: prevstate.count + this.props.addvalue
//     }))
//     // this.state.count = this.state.count + 1
//     // console.log(this.state.count);
// }
// incrementFive(){
//     this.increment()
//     this.increment()
//     this.increment()
//     this.increment()
//     this.increment()
// }

//   render() {
//     return (
//       <div>
//         <div>
//             count - {this.state.count}
//         </div>
//         <button onClick={()=>this.increment()}>Increment</button>
//         <button onClick={()=>this.incrementFive()}>incrementFive</button>
//       </div>
//     )
//   }
// }

function Counter(props){
  console.log({props});
    const [count, setCount] = useState(0);
    const increment = () => {
      setCount(prevCount => prevCount + parseInt(props.addvalue));
    };
    const decrement = () => {
      setCount(prevCount => prevCount - parseInt(props.subvalue));
    };
    return (
        <div>
            <p>You clicked {count} times</p>
            <button onClick={ () => setCount(count + props.addvalue)}>Increment</button>
            <button onClick={() => setCount(count - props.subvalue)}>Decrement</button>
            <button onClick={ () => {setCount(0)}}>Reset</button>
        </div>
      )

}

export default Counter
