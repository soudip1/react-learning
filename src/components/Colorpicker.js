import React from "react";
import { SketchPicker } from "react-color";
import { useState } from "react";
function Colorpicker() {
  //creating state to store our color and also set color using onChange event for sketch picker
  const [sketchPickerColor, setSketchPickerColor] = useState({
    r: "241",
    g: "112",
    b: "19",
    a: "1",
  });
  // destructuring rgba from state
  const { r, g, b, a } = sketchPickerColor;

  return (
    <div
      className="App"
      style={{ display: "flex", justifyContent: "space-around" }}
    >
      <div className="sketchpicker">
        <h2>Sketch Picker</h2>
        {/* Div to display the color  */}
        <div
          style={{
            backgroundColor: `rgba(${r},${g},${b},${a})`,
            width: 100,
            height: 50,
            border: "2px solid white",
          }}
        ></div>
        {/* Sketch Picker from react-color and handling color on onChange event */}
        <SketchPicker
          onChange={(color) => {
            setSketchPickerColor(color.rgb);
          }}
          color={sketchPickerColor}
        />
      </div>
    </div>
  );
}

export default Colorpicker;
