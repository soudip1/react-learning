import React from 'react';
import { useParams } from 'react-router-dom';

const Userdetail = () => {
    const { userId } = useParams()
    // const  = params.userId 
    return (
        <div>
            details about user {userId}.
        </div>
    );
};

export default Userdetail;