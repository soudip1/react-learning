import React, { Component } from "react";
class Message extends Component {
    constructor(){
        super()
        this.state = {
            message: 'Hello world'
        }

    }
        changeMessage() {
            this.setState({
                message: 'Thank You.'
            })
        }
  render() {
    return (
      <div class = "table">
        <h2 className="familyVlog">{this.state.message}</h2>
        <button onClick={() => this.changeMessage()}>Change Text</button>
      </div>
    );
  }
}
export default Message;
