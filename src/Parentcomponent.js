import { useState } from "react";
import Childcomponent from "./Childcomponent";

const Parentcomponent = () => {
  const [parent, setParent] = useState(
    { parentname: "parent" },
    { childname: "" }
  );
  const greetParent = (name) => {
  
    setParent((previousObjet)=> ({...previousObjet , childname:name, lastName: 'Kumar'}));
  };
  console.log("Hello", parent.parentname, "from", parent.childname , parent.lastName);
  return (
    <div>
      <Childcomponent greetParent={greetParent}></Childcomponent>
    </div>
  );
};

export default Parentcomponent;
